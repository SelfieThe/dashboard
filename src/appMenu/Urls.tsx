export const Urls = () => (
  {
    dashboard: {home: '/', prebuild: 'prebuild'},
    apps: {page1: '/page1', page2: '/page2', page3: '/page3'},
    listMenu: {page4: '/page4',page5: '/page5', sublistmenu: {page6: '/sub/page6', page7: '/sub/page7'}}
  }
)