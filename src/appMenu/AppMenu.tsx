import React from "react"
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import { Link } from "react-router-dom";
import Grid from "@mui/material/Grid";
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import { Urls } from "./Urls";

export const AppMenu = () => {
  const [active, setActive]:any = React.useState({dashboard: true, apps: false, listMenu: false, subListMenu: false})


  const [dashboardEl, setDashboardEl] = React.useState<null | HTMLElement>(null);
  const openDashboard = Boolean(dashboardEl);
  const handleDashboardClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setDashboardEl(event.currentTarget);
    setActive({...active, 
      dashboard: true,
      apps: false,
      listMenu: false,
      subListMenu: false
    })
  };
  const handleDashboardClose = () => {
    setDashboardEl(null);
  };



  const [appsEl, setAppsEl] = React.useState<null | HTMLElement>(null);
  const openApps = Boolean(appsEl);
  const handleAppsClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAppsEl(event.currentTarget);
    setActive({...active, 
      dashboard: false,
      apps: !active.apps,
      listMenu: false,
      subListMenu: false
    })
    };
    const handleAppsClose = () => {
      setAppsEl(null);
    }; 
    
    
  const [listMenuEl, setListMenuEl] = React.useState<null | HTMLElement>(null);
  const openListMenu = Boolean(listMenuEl);
  const handleListMenuClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setListMenuEl(event.currentTarget);
    setActive({...active, 
      dashboard: false,
      apps: false,
      listMenu: !active.listMenu,
      subListMenu: false
    })
    };
    const handleListMenuClose = () => {
      setListMenuEl(null);
    }; 




  const [subListMenuEl, setSubListMenuEl] = React.useState<null | HTMLElement>(null);
  const openSubListMenu = Boolean(subListMenuEl);
  const handleSubListMenuClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setSubListMenuEl(event.currentTarget);
    setActive({...active, 
      dashboard: false,
      apps: false,
      listMenu: false,
      subListMenu: !active.listMenu
    })
    };
    const handleSubListMenuClose = () => {
      setSubListMenuEl(null);
    }; 



  return (
    <Grid container direction="row">
      <div>
        <Button
          id="basic-button"
          aria-controls={openDashboard ? 'basic-menu' : undefined}
          aria-haspopup="true"
          aria-expanded={openDashboard ? 'true' : undefined}
          style={{color: '#fff', backgroundColor: active.dashboard ? 'rgba(255,255,255, 0.08)' : ''}}
          onClick={handleDashboardClick}
        >
          Dashboard
        </Button>
        <MyMenu 
          anchorEl={dashboardEl} 
          open={openDashboard} 
          onClose={handleDashboardClose} 
          element={
            <div key="appsMenuList">
              <Btn active={active} link={Urls().dashboard.home} element={<MenuItem onClick={handleDashboardClose}>Home</MenuItem>}/>
              <Btn active={active} link={Urls().dashboard.prebuild} element={<MenuItem onClick={handleDashboardClose}>PreBuild</MenuItem>}/>
            </div>
          }>

        </MyMenu>
      </div>
      <div>
      <Button
        id="basic-button"
        aria-controls={openApps ? 'basic-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={openApps ? 'true' : undefined}
        style={{color: '#fff', backgroundColor: active.apps ? 'rgba(255,255,255, 0.08)' : ''}}
        onClick={handleAppsClick}
      >
        Apps
      </Button>
      <MyMenu 
        anchorEl={appsEl} 
        open={openApps} 
        onClose={handleAppsClose} 
        element={
          <div key="appsMenuList">
            <Btn active={active} link={Urls().apps.page1} element={<MenuItem onClick={handleAppsClose}>Page1</MenuItem>}/>
            <Btn active={active} link={Urls().apps.page2} element={<MenuItem onClick={handleAppsClose}>Page2</MenuItem>}/>
            <Btn active={active} link={Urls().apps.page3} element={<MenuItem onClick={handleAppsClose}>Page3</MenuItem>}/>
          </div>
        }> 
      </MyMenu>
    </div>
    <div>
      <Button
        id="basic-button"
        aria-controls={openListMenu ? 'basic-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={openListMenu ? 'true' : undefined}
        style={{color: '#fff', backgroundColor: active.listMenu ? 'rgba(255,255,255, 0.08)' : ''}}
        onClick={handleListMenuClick}
      >
        ListMenu
      </Button>
      <MyMenu 
        anchorEl={listMenuEl} 
        open={openListMenu} 
        onClose={handleListMenuClose} 
        element={
          <div key="subMenuList">
            <Btn active={active} link={Urls().listMenu.page4} element={<MenuItem onClick={handleListMenuClose}>Page4</MenuItem>}/>
            <Btn active={active} link={Urls().listMenu.page5} element={<MenuItem onClick={handleListMenuClose}>Page5</MenuItem>}/>
                    
            <Button
              id="basic-button"
              aria-controls={openSubListMenu ? 'basic-menu' : undefined}
              aria-haspopup="true"
              aria-expanded={openSubListMenu ? 'true' : undefined}
              style={{ backgroundColor: active.subListMenu ? 'rgba(255,255,255, 0.08)' : '', marginLeft: 15}}
              onClick={handleSubListMenuClick}
            >        
              SubListMenu
              {openSubListMenu ? <ExpandLess /> : <ExpandMore />}
            </Button>
          
            <MyMenu 
              anchorEl={subListMenuEl} 
              open={openSubListMenu} 
              onClose={handleSubListMenuClose} 
              element={
                <div key="sublistMenuList">
                  <Btn active={active} link={Urls().listMenu.sublistmenu.page6} element={<MenuItem onClick={handleSubListMenuClose}>Page6</MenuItem>}/>
                  <Btn active={active} link={Urls().listMenu.sublistmenu.page7} element={<MenuItem onClick={handleSubListMenuClose}>Page7</MenuItem>}/>
                </div>
              }>
            </MyMenu>
          </div>
        }>
        </MyMenu>
    </div>
  </Grid>
  )
}



const Btn = (props: any) => (
  <Button       
    id="basic-button" 
    aria-controls='basic-menu'
    style={{width: "100%", display: 'flex', justifyContent: 'left'}}
  >
    <Link to={props.link} style={{textDecoration: 'none'}}>
      {props.element}
    </Link>
  </Button>
)


const MyMenu = (props: any) => ( 
  <Menu
    id="basic-menu"
    anchorEl={props.anchorEl}
    open={props.open}
    onClose={props.onClose}
    MenuListProps={{
      'aria-labelledby': 'basic-button',
    }}
    style={{width: 180}}
  >
    {props.element}
  </Menu>
) 