import React from 'react';
import { MyDateRangePicker } from './components/DataRangePicker';
import { Table } from './components/Table';

export const PreBuild = () => {
  return (
    <div>
      <MyDateRangePicker />
      <Table />
    </div>
  );
};


