import React from 'react';
import TextField from '@mui/material/TextField';
import { LocalizationProvider } from '@mui/x-date-pickers-pro';
import { AdapterDateFns } from '@mui/x-date-pickers-pro/AdapterDateFns';
import { DateRangePicker, DateRange } from '@mui/x-date-pickers-pro/DateRangePicker';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';

export const MyDateRangePicker = () => {
// eslint-disable-next-line react-hooks/rules-of-hooks
const [value, setValue] = React.useState<DateRange<Date>>([null, null]); 
return (  
  <Container maxWidth={false} style={{width: '100%'}}>
    <Grid
      container
      item xs={12}
    >
      <Grid
        item
        style={{display: 'flex', justifyContent: 'center', width: "100%"}}
      >
        <Box
          style={{display: 'flex', alignItems: 'center'}}
          sx={{
            height: 100,
            position: 'relative'
          }}
        >
          <LocalizationProvider
            dateAdapter={AdapterDateFns}
            localeText={{ start: 'Check-in', end: 'Check-out' }}
          >
            <DateRangePicker
              value={value}
              onChange={(newValue) => {
                setValue(newValue);
              }}
              renderInput={(startProps, endProps) => {
                return (
                  <>
                    <TextField {...startProps } />
                    <Box sx={{ mx: 2 }}> to </Box>
                    <TextField {...endProps} />
                  </>
                )}
              }
            />
          </LocalizationProvider>
        </Box>
      </Grid>
    </Grid>
  </Container>
)}