import { DataGrid } from '@mui/x-data-grid';

export const Table = () =>  {
  const data: any = {
    columns: [
      {field: 'id', headerName: 'ID', width: 100},
      {field: 'head1', headerName: 'Head1', width: 200},
    ],
    rows: [
      {id: 1, head1: 'text'},
      {id: 2, head1: 'asd'},
      {id: 3, head1: 'dhrhrh'},
      {id: 4, head1: 'wetwt'},
    ]
  }
  return (
    <div style={{ height: 400, width: '100%' }}>
      <DataGrid rowHeight={25} {...data} />
    </div>
  );
}