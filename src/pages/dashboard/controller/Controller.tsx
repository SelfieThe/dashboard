import React from "react";

export const Controller = () => {
  const [typeChart, setTypeChart] = React.useState('Line')
  const handleChangeTypeChart = (e: any) => {    
    setTypeChart(e.target.value);
  }
  const ControllerTypeChart = { typeChart, handleChangeTypeChart }

  const [stateMonth, setStateMonth] = React.useState(false)

  const handleChangeMonthState = () => {
    setStateMonth(!stateMonth)
  }
  const ControllerMonthChecker = { stateMonth, handleChangeMonthState }


  return { ControllerTypeChart, ControllerMonthChecker }
}
