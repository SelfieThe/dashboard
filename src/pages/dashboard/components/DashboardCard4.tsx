import { Avatar, Card, CardContent, Grid, Typography } from '@mui/material';
import AttachMoneyIcon from '@mui/icons-material/AttachMoney';

export const DashboardCard4 = () => {
  return (
    <Card sx={{ height: '100%' }}>
      <CardContent>
        <Grid
          container
          spacing={3}
          sx={{ justifyContent: 'space-between' }}
        >
          <Grid item>
            <Typography
              color="textSecondary"
              gutterBottom
              variant="overline"
            >
              CARD4
            </Typography>
            <Typography
              color="textPrimary"
              variant="h4"
            >
              info text
            </Typography>
          </Grid>
          <Grid item>
            <Avatar
              sx={{
                backgroundColor: 'primary.main',
                height: 56,
                width: 56
              }}
            >
              icon
              {/* <AttachMoneyIcon /> */}
            </Avatar>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  )
}
