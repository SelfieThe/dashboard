import React from 'react';
import { Box, Card, CardContent, CardHeader, Divider, Grid } from '@mui/material';
import TextField from '@mui/material/TextField';
import { Controller } from '../controller/Controller';
import { ChartSwitcher } from './ChartSwitcher';
import { ChartLine } from './ChartLine';
import { ChartBar } from './ChartBar';
import { MonthChecker } from './ChartMonthChecker';

export const DashboardChart1 = (props: any) => {
  const ControllerTypeChart         = Controller().ControllerTypeChart
  const ControllerMonthChecker      = Controller().ControllerMonthChecker
  const currentDate:Date            = new Date();
  const cDay                        = currentDate.getDate()
  const cMonth                      = currentDate.getMonth() + 1
  const cYear                       = currentDate.getFullYear()

  const [ firstDate, setFirstDate ] = React.useState(`${cYear}-0${cMonth}-${cMonth>9? '':'0'}${cDay-cDay+1}`)
  const [ lastDate,  setLastDate  ] = React.useState(`${cYear}-0${cMonth}-${cDay>9? "":"0"}${cDay}`)

  const [ chartData, setChartData ]: any = React.useState([
    {count: 1,  date: '01-01-2022'},
    {count: 4,  date: '02-01-2022'},
    {count: 25, date: '03-01-2022'},
    {count: 10, date: '04-01-2022'},
  ])

  const handleFirstDateChange = (e:any) => {
    setFirstDate(e.target.value)
  }

  const handleLastDateChange = (e:any) => {  
    setLastDate(e.target.value)
  }

  return (
    <Card>
      <CardHeader
        title={props.title}
      />
      <Divider />
      <CardContent>
        <Box
          sx={{
            height: 400,
            position: 'relative'
          }}
        >
          {ControllerTypeChart.typeChart === "Line" ? 
            <ChartLine data={chartData}/> : <ChartBar data={chartData}/>
          }
        </Box>
      </CardContent>
      <Divider />
      <Box sx={{
        display: 'flex',
        alignItems: 'center'
      }}>
        <Box>
          {ChartSwitcher(ControllerTypeChart)} 
          {MonthChecker(ControllerMonthChecker)}
        </Box>   
        <Grid container>
          <Grid item container direction="row" style={{display: 'flex', justifyContent: 'flex-end'}}>
            <Grid item style={{padding: '5px 2px 5px 0px'}}>
              <TextField
                id              =    "firstdate"
                label           =    "FirstDate"
                type            =    "date"
                value           = {  firstDate }
                onChange        = {  handleFirstDateChange }            
                InputLabelProps = {{ shrink: true }}
              />
            </Grid>
            <Grid item style={{padding: '5px 2px 5px 0px'}}>
              <TextField
                id              =    "lastdate"
                label           =    "LastDate"
                type            =    "date"        
                value           = {  lastDate }
                onChange        = {  handleLastDateChange }        
                InputLabelProps = {{ shrink: true }}
              />
            </Grid> 
          </Grid>
        </Grid>
      </Box>
    </Card>
  );
};
