import React      from "react"
import Checkbox   from "@mui/material/Checkbox"
import Grid from "@mui/material/Grid"
import FormControlLabel from "@mui/material/FormControlLabel"

export const MonthChecker = (Config: any) => {
  return (
    <Grid container>
      <FormControlLabel 
        value   = {false} 
        control = { <Checkbox
          checked={Config.stateMonth}
          onChange={()=>Config.handleChangeMonthState()}
        /> } 
        label = "По месяцам" 
      />
    </Grid>
  )
}
