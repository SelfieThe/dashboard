import FormControlLabel from '@mui/material/FormControlLabel'
import Grid from '@mui/material/Grid'
import Radio from '@mui/material/Radio/Radio'
import RadioGroup from '@mui/material/RadioGroup'
import React            from 'react'


export const ChartSwitcher = (ConfigChartSwitcher: any) => {
  return (
    <Grid container>
      <RadioGroup  
        value={ConfigChartSwitcher.typeChart} 
        onChange={ConfigChartSwitcher.handleChangeTypeChart}           
      >
        <Grid item container direction="row">
          <Grid item>
            <FormControlLabel 
              value   =   "Line" 
              control = { <Radio /> } 
              label   =   "Линейный" 
            />
          </Grid>
          <Grid item>
            <FormControlLabel 
              value   =   "Bar" 
              control = { <Radio /> } 
              label   =   "Гистограмма" 
            />
          </Grid>
        </Grid>
      </RadioGroup>  
    </Grid>
  )
}
