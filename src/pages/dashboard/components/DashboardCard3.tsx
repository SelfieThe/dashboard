import React from 'react';
import { Avatar, Box, Button, Card, CardContent, Grid, LinearProgress, Typography } from '@mui/material';
import InsertChartIcon from '@mui/icons-material/InsertChartOutlined';

export const DashboardCard3 = () => {
  const [progress, setProgress] = React.useState<any>('50')
  return (
    <Card sx={{ height: '100%' }}>
      <CardContent>
        <Grid
          container
          spacing={3}
          sx={{ justifyContent: 'space-between' }}
        >
          <Grid item>
            <Typography
              color="textSecondary"
              gutterBottom
              variant="overline"
            >
              CARD PROGRESS
            </Typography>
            <Typography
              color="textPrimary"
              variant="h4"
            >
              {progress}%
            </Typography>
          </Grid>
          <Grid item>
            <Avatar
              sx={{
                backgroundColor: 'warning.main',
                height: 56,
                width: 56
              }}
            >
              icon
              {/* <InsertChartIcon /> */}
            </Avatar>
          </Grid>
        </Grid>
        <Box sx={{ pt: 3 }}>
          <LinearProgress
            value={parseInt(progress)}
            variant="determinate"
          />
        </Box>
        <Box sx={{ pt: 3, display: 'flex', justifyContent: 'center' }}>
          <Button onClick = {() => setProgress(progress > 0 ? progress - 1 : 100)}> - </Button>
          <Button onClick = {() => setProgress(progress < 100 ? parseInt(progress) + 1 : 0)}> + </Button>
        </Box>
      </CardContent>
    </Card>
  )
}
