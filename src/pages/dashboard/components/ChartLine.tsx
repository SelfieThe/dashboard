import Grid from "@mui/material/Grid"
import Typography from "@mui/material/Typography"
import React from "react"
import { CartesianGrid, Line, LineChart, ResponsiveContainer, Tooltip, XAxis } from "recharts"

export const ChartLine = (props: any) => {
  return ( 
    <>
    {props.data.length > 0 ? 
      <ResponsiveContainer width="100%" height="100%">
        <LineChart
          key    = {1}
          data   = {props.data}
          margin = {{ top: 0, right: 20, left: 0, bottom: 10 }}          
        >
          <Line 
            type    = "monotone" 
            dataKey = "count" 
            stroke  = "#8884d8" 
          />
          <XAxis dataKey="date" />
          <CartesianGrid 
            stroke          = "#ccc" 
            strokeDasharray = "5 5" 
          />
          <Tooltip />
        </LineChart> 
      </ResponsiveContainer>
      : 
      <ResponsiveContainer width="100%" height="100%">
        <Grid style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
          <Typography>Нет данных</Typography>
        </Grid>
      </ResponsiveContainer>}
    </>
  )
}