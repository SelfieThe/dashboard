import Grid from "@mui/material/Grid"
import Typography from "@mui/material/Typography"
import React from "react"
import { CartesianGrid, Bar, BarChart, ResponsiveContainer, Tooltip, XAxis } from "recharts"

export const ChartBar = (props: any) => {
  return ( 
    <>
    {props.data.length > 0 ? 
      <ResponsiveContainer width="100%" height="100%">
        <BarChart
          key    = {1}
          data   = {props.data}
          margin = {{ top: 0, right: 20, left: 0, bottom: 10 }}          
        >
          <Bar 
            type    = "monotone" 
            dataKey = "count" 
            fill  = "#8884d8" 
          />
          <XAxis dataKey="date" />
          <CartesianGrid 
            stroke          = "#ccc" 
            strokeDasharray = "5 5" 
          />
          <Tooltip />
        </BarChart> 
      </ResponsiveContainer>
      : 
      <ResponsiveContainer width="100%" height="100%">
        <Grid style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
          <Typography>Нет данных</Typography>
        </Grid>
      </ResponsiveContainer>}
    </>
  )
}