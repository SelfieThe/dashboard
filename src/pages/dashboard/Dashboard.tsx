import React from "react";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import { DashboardCard1 } from "./components/DashboardCard1";
import { DashboardCard2 } from "./components/DashboardCard2";
import { DashboardCard3 } from "./components/DashboardCard3";
import { DashboardCard4 } from "./components/DashboardCard4";
import { DashboardChart1 } from "./components/DashboardChart1";
import { DashboardChart2 } from "./components/DashboardChart2";

export const Dashboard = () => {
  return (
    <Box
      component="main"
      sx={{
        flexGrow: 1,
        py: 8
      }}
    >
      <Container maxWidth={false}>
        <Grid
          container
          spacing={3}
        >
          <Grid
            item
            lg={3}
            sm={6}
            xl={3}
            xs={12}
          >
            <DashboardCard1 />
          </Grid>
          <Grid
            item
            xl={3}
            lg={3}
            sm={6}
            xs={12}
          >
            <DashboardCard2 />
          </Grid>
          <Grid
            item
            xl={3}
            lg={3}
            sm={6}
            xs={12}
          >
            <DashboardCard3 />
          </Grid>
          <Grid
            item
            xl={3}
            lg={3}
            sm={6}
            xs={12}
          >
            <DashboardCard4 />
          </Grid>
          <Grid
            item
            lg={8}
            md={12}
            xl={9}
            xs={12}
          >
            <DashboardChart1 title="ChartTitle"/>
          </Grid>
          <Grid
            item
            lg={4}
            md={12}
            xl={3}
            xs={12}
          >
            <DashboardChart2 />
          </Grid>
        </Grid>
      </Container>
    </Box>
  )
}